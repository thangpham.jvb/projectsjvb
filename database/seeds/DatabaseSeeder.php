<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name'=>'Pham Minh Thang',
            'username'=>'admin',
            'password'=>bcrypt('123456'),
            'status'=>'1'
        ]);
    }
}
