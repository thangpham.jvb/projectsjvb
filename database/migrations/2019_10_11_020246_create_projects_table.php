<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('keyword');
            $table->text('shortDescription');
            $table->text('detailDescription');
            $table->string('nation');
            $table->tinyInteger('devQuantity');
            $table->tinyInteger('brSeQuantity');
            $table->tinyInteger('status');
            $table->date('startDate');
            $table->date('endDate');
            $table->timestamps();

            //foreign key for customer id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
