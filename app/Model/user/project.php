<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    public function tags()
    {
    	return $this->belongsToMany('App\Model\user\tag','project_tags')->withTimestamps();
    }

    public function categories()
    {
    	return $this->belongsToMany('App\Model\user\category','category_projects')->withTimestamps();;
    }

    public function getRouteKeyName()
    {
    	return 'slug';
    }
}
