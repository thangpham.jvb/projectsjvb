<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Model\User\project;
use App\Model\User\category;
use App\Model\User\tag;


class ProjectController extends Controller
{
     // Middleware for Admin
     public function __construct()
     {
         $this->middleware('auth:admin');
     }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = project::orderBy('created_at','DESC')->get();
        return view('admin.project.index',compact('projects')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags =tag::all();
        $categories =category::all();
        return view('admin.project.create',compact('tags','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
            'image' => 'nullable|max:1999',
            ]);

        //Handle file upload
        if($request->hasFile('image'))
        {
            // Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        } else
        {
            $fileNameToStore = 'noimage.jpg';
        }

        //Create new project
        $project = new project;
        $project->image = $fileNameToStore;
        $project->title = $request->title;
        $project->subtitle = $request->subtitle;
        $project->slug = $request->slug;
        $project->body = $request->body;
        $project->status = $request->status;
        $project->save();
        // Many to many relation between projects and Tags
        $project->tags()->sync($request->tags);
        // Many to many relation between projects and Categories
        $project->categories()->sync($request->categories);
        return redirect(route('project.index'))->with('message', 'Added project Successfully!!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = project::with('tags','categories')->where('id',$id)->first();
        $tags =tag::all();
        $categories =category::all();
        return view('admin.project.edit',compact('tags','categories','project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
            'image'=>'nullable|max:1999'
            ]);
       //Handle file upload
        if($request->hasFile('image'))
        {
            // Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        } else
        {
            $fileNameToStore = 'noimage.jpg';
        }
        //Update file
        $project = project::find($id); 
        $project->title = $request->title;
        $project->subtitle = $request->subtitle;
        $project->slug = $request->slug;
        $project->body = $request->body;
        $project->status = $request->status;
        if($request->hasFile('image'))
        {
            // Delete the old image if it's changed .
            if ($project->image != 'no_image.png') 
            {
                Storage::delete('public/images/'.$project->image);
            }
            $project->image = $fileNameToStore;
        }
        $project->tags()->sync($request->tags);
        $project->categories()->sync($request->categories);
        $project->save();
        return redirect(route('project.index'))->with('message', 'Updated project Successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects = project::find($id);
        //Delete image from project
        if($projects->image != 'noimage.jpg')
        {
            Storage::delete('public/images/'.$projects->image);
        }
        //$projects->categories()->detach();
        //$projects->tags()->detach();
        $projects->delete();
        return redirect(route('project.index'))->with('message', 'Deleted project Successfully!!!!');
    }
}
